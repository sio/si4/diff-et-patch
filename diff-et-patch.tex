% diff et patch pour la transmission de correctifs et/ou améliorations
%
% Auteur : Grégory DAVID
%%

\documentclass[12pt,a4paper,oneside,titlepage,final]{article}

\usepackage{style/layout}
\usepackage{style/glossaire}
\newcommand{\BASHDIR}{data}
\usepackage[%
hide=true,%
dir={\BASHDIR},%
]{bashful}

\newcommand{\MONTITRE}{\texttt{diff} et \texttt{patch}}
\newcommand{\MONSOUSTITRE}{identification des modifications \\ et application de correctifs}
\newcommand{\DISCIPLINE}{\glsentrytext{SiQUATRE} -- \glsentrydesc{SiQUATRE}}

\usepackage[%
	pdftex,%
	pdfpagelabels=true,%
	pdftitle={diff et patch},%
	pdfauthor={Grégory DAVID},%
        colorlinks,%
]{hyperref}
\usepackage{style/commands}

\title{%
  \begin{flushright}
      \noindent{\Huge {\bf \MONTITRE}} \\
      \noindent{\huge \MONSOUSTITRE} \\
      \noindent{\large \DISCIPLINE} \\
  \end{flushright}
}

\begin{document}
% Page de titre
\maketitle

% Copyright
\include{copyright}

% Contenu
\HEADER{\MONTITRE}{\DISCIPLINE}

\section{Présentation du problème}\label{sec:presentation.probleme}
Considérons le fichier de base, nommé \texttt{fichier.original}, reçu d'un
auteur et présenté dans le
\lstlistingname~\vref{lst:fichier.original}. Ce fichier contient du
contenu avec des fautes d'orthographe, précisément sur les accords au
pluriel de certains mots, et il manque la référence à la source du
document.

Considérons par la suite que ce fichier \texttt{fichier.original}, est
modifié et sauvegardé dans un fichier \texttt{fichier.modification},
tel que présenté dans le
\lstlistingname~\vref{lst:fichier.modification}.

\lstset{basicstyle={\scriptsize\ttfamily}}
\lstinputlisting[label={lst:fichier.original},caption={Contenu initial
  du fichier dans
  \texttt{fichier.original}}]{\BASHDIR/fichier.original}
\lstinputlisting[label={lst:fichier.modification},caption={Contenu
  \textbf{modifié} du \texttt{fichier.original} stocké dans le fichier
  \texttt{fichier.modification}}]{\BASHDIR/fichier.modification}

\section{Identification des
  modifications}\label{sec:identification.modifications}

La question principale qui se pose ici est :
\begin{quotation}
  \textbf{Comment connaître les modifications qui ont été apportées au
    fichier original ?}
\end{quotation}

La commande \texttt{diff}\footnote{voir la page de manuel de la
  commande : \texttt{man diff}} permet d'établir les différences qui
existent entre les lignes de deux fichiers textuels.

Par conséquent, il faut s'assurer d'avoir une copie de la version
originale du fichier (dans \texttt{fichier.original}) avant toute
modification afin d'avoir la base de référence pour établir la
comparaison. Les modifications sont alors sauvegardées dans le fichier
nommé \texttt{fichier.modification}.

Le \lstlistingname~\vref{lst:diff.commande} présente un appel de la
commande \texttt{diff}, paramétré avec \texttt{-u} et les deux noms de
fichiers à comparer, fournissant alors en sortie standard le
\lstlistingname~\vref{lst:diff.fichier.original.vs.modification}
correspondant aux différences entre les deux fichiers.

\bash[prefix={}]
diff -u fichier.original fichier.modification
\END

\lstinputlisting[%
label={lst:diff.commande},%
caption={Invocation du programme de différence entre
  \texttt{fichier.original} et
  \texttt{fichier.modification}}]{\BASHDIR/\jobname.sh}

\lstinputlisting[%
language={diff},%
label={lst:diff.fichier.original.vs.modification},%
caption={Sortie unifiée de la différence \texttt{diff} entre
  \texttt{fichier.original} et
  \texttt{fichier.modification}}]{\BASHDIR/\jobname.stdout}

\subsection{Format de sortie}\label{sec:format-de-sortie}
\subsubsection{En-tête}\label{sec:header}

L'en-tête contient des informations (nom des fichiers, dates et heures
des modifications) quant aux fichiers concernés par la comparaison
(\texttt{fichier.original} et \texttt{fichier.modification} dans notre
cas).

\lstinputlisting[linerange={1-2},%
language={diff}]{\BASHDIR/\jobname.stdout}

\subsubsection{Contexte}\label{sec:contexte}

Le contexte permet de connaître les différentes lignes concernées par
les modifications.

\lstinputlisting[linerange={3-3},firstnumber=3,%
language={diff}]{\BASHDIR/\jobname.stdout}

\subsubsection{Laissée intacte}\label{sec:laisse-intact}

Toute ligne ne subissant aucune modification, ajout ou suppression est
considérée comme ayant été laissée intacte. Elle est préfixée du
caractère espace (et apparaît toujours en noir dans les sorties
colorées, comme avec la commande \texttt{colordiff}).

Notez que seules quelques lignes intactes peuvent être affichées afin
d'établir un contexte de modification, mais on ne conserve pas toutes
les lignes intactes, puisqu'elles sont intactes !

\lstinputlisting[linerange={13-13},firstnumber=13,%
language={diff}]{\BASHDIR/\jobname.stdout}

\subsubsection{Ajout}\label{sec:ajout}

Un ajout est symbolisé par une ligne préfixée d'un
\texttt{\color{green}+}, qu'elle soit vide ou non, et la couleur verte
est souvent employée pour le caractériser.

\lstinputlisting[linerange={23-24},firstnumber=23,%
language={diff}]{\BASHDIR/\jobname.stdout}

\subsubsection{Suppression}\label{sec:suppression}

Une suppression est symbolisée par une ligne préfixée par un
\texttt{\color{red}-}, qu'elle soit vide ou non, et la couleur rouge
est souvent employée pour la caractériser.

\lstinputlisting[linerange={4-4},firstnumber=4,%
language={diff}]{\BASHDIR/\jobname.stdout}

\subsubsection{Modification}\label{sec:modification}

Une modification est la succession d'une suppression et d'un ajout.

\lstinputlisting[linerange={6-9},firstnumber=6,%
language={diff}]{\BASHDIR/\jobname.stdout}

\section{Transmission et application des
  correctifs}\label{sec:transmission.application.correctifs}

Lorsqu'une différence, à l'aide de la commande \texttt{diff}, a été
réalisée et stockée dans un fichier, disons \texttt{fichier.patch} --
que l'on nomme communément le fichier de correctif, ou bien
\emph{patch} en anglais --, il semble intéressant de pouvoir appliquer
les modifications de manière automatique sur le fichier original.

Pour effectuer l'enregistrement du correctif, nous utilisons les
redirections d'entrée/sortie du système d'exploitation.

\paragraph{Constitution du correctif} ~

\bash[prefix={},script]
diff -u fichier.original fichier.modification >fichier.patch
\END

De fait, si nous voulons proposer nos modifications à l'auteur du
\texttt{fichier.original}, nous pouvons lui envoyer par courrier
électronique (ou tout autre moyen numérique) le fichier contenant les
correctifs : \texttt{fichier.patch}

De son côté l'auteur n'a plus qu'à appliquer le correctif
\texttt{fichier.patch} (voir
\lstlistingname~\vref{lst:diff.fichier.original.vs.modification}),
après l'avoir relu et validé bien évidemment, sur sa propre version du
fichier original : \texttt{fichier.original}

\bash[prefix={}]
cp fichier.original fichier.original.bak
mv fichier.modification fichier.modification.bak
\END

\lstinputlisting[label={lst:fichier.avant.patch}, caption={Contenu du
  \texttt{fichier.original} de l'auteur original avant l'application
  du correctif}]{\BASHDIR/fichier.original}

\paragraph{Application du correctif} ~

\bash[prefix={},script]
patch <fichier.patch
\END

\lstinputlisting[label={lst:fichier.apres.patch}, caption={Contenu du
  \texttt{fichier.original} de l'auteur original \textbf{après}
  l'application du correctif}]{\BASHDIR/fichier.original}

\bash[prefix={}]
mv fichier.original.bak fichier.original
mv fichier.modification.bak fichier.modification
rm fichier.patch
\END

\end{document}
